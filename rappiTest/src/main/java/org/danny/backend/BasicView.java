/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.danny.backend;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="dtBasicView") 
@ViewScoped
public class BasicView implements Serializable {
    

    @PostConstruct
    public void init() {
       
    }
    
	
	private String inputString;

	private String outputString;

	
//	public static void main(String[] args) {
//		
//		BasicView defe=new BasicView();
//		defe.setInputString("2\n"+
//"4 5\n"+
//"UPDATE 2 2 2 4\n"+
//"QUERY 1 1 1 3 3 3\n"+
//"UPDATE 1 1 1 23\n"+
//"QUERY 2 2 2 4 4 4\n"+
//"QUERY 1 1 1 3 3 3\n"+
//"2 4\n"+
//"UPDATE 2 2 2 1\n"+
//"QUERY 1 1 1 1 1 1\n"+
//"QUERY 1 1 1 2 2 2\n"+
//"QUERY 2 2 2 2 2 2 \n");
//		defe.processValue();
//		System.out.println(defe.getOutputString());
//	}


	//Ejecutar el main para probar la solución
	public void processValue() {
		try{
			FastScanner scan = new FastScanner(inputString);
			int t=scan.nextInt();

			StringBuilder outputAnswer=new StringBuilder();
			outputAnswer.append("Valor T = "+t+"\n");
			for(int i=0;i<t;i++){
				int n,m;
				try{
					n=scan.nextInt();
					m=scan.nextInt();
					outputAnswer.append("Valor N = "+n);
					outputAnswer.append(" Valor M = "+m+" \n");
				}catch(Exception e){
					outputAnswer.append("Error interpretando el valor de T(Casos de prueba) \n");
					break;
				}
				
				HashMap<String,Integer> mapaCoordenadas=new HashMap<String,Integer>();
				for(int j=0;j<m;j++){
					String[] line=null;
					try{
						line=scan.nextLine().split(" ");
					}catch(Exception e){
						outputAnswer.append("Error interpretando el valor de M(Numero de operaciones) \n");
						break;
					}
					
					if(line[0].equals("UPDATE")){
						actualizarValor(line,mapaCoordenadas);
					}else{
						String sumResult=sumarValoresCubo(line,mapaCoordenadas)+"";
						outputAnswer.append(sumResult+"\n");
					}
				}
			}
			outputString=outputAnswer!=null?outputAnswer.toString():"";
		}catch(Exception e){
			outputString="";
		}
		
	}


	/**
	 * Método auxiliar
	 */
	public static boolean seEncuentraEnCubo(String s,int x1,int y1,int z1,int x2,int y2,int z2){ 
		String[] com=s.split(" ");
		int x=Integer.parseInt(com[0]),y=Integer.parseInt(com[1]),z=Integer.parseInt(com[2]);
		return(x>=x1&&x<=x2&&y>=y1&&y<=y2&&z>=z1&&z<=z2);
	}

	/**
	 * Suma todos los valores del mapa dentro de un intervalo de coordenadas
	 * @param com
	 * @param mapaCoordenadas
	 * @return
	 */
	public static long sumarValoresCubo(String[] com,HashMap<String,Integer> mapaCoordenadas){
		int x1=Integer.parseInt(com[1]),y1=Integer.parseInt(com[2]),z1=Integer.parseInt(com[3]); //Extraccion de primera coordenada
		int x2=Integer.parseInt(com[4]),y2=Integer.parseInt(com[5]),z2=Integer.parseInt(com[6]); //Extraccion de segunda coordenada
		long sum=0;

		for(Entry<String, Integer> p:mapaCoordenadas.entrySet()){
			if(seEncuentraEnCubo(p.getKey(),x1,y1,z1,x2,y2,z2)){
				sum+=p.getValue();										//En caso de que el valor se encuentre en el cubo se suma el valor al total
			}
		}
		return sum;
	}

	/**
	 * Realiza un set de un valor determinado
	 * @param com
	 * @param mapaCoordenadas
	 */
	public static void actualizarValor(String[] com,HashMap<String,Integer> mapaCoordenadas){
		String a=com[1]+" "+com[2]+" "+com[3];
		int W=Integer.parseInt(com[4]);
		mapaCoordenadas.put(a, W); //Hace el set del valor en el mapa
	}



	public String getInputString() {
		return inputString;
	}


	public void setInputString(String inputString) {
		this.inputString = inputString;
	}


	public String getOutputString() {
		return outputString;
	}


	public void setOutputString(String outputString) {
		this.outputString = outputString;
	}





	//-----------FastScanner class for faster input---------------------------------- Esto fue tomado de la siguiente URL http://www.java-made-easy.com/java-scanner.html
	/**
	 * Clase auxiliar (INPUT DE DATA)
	 * @author danny
	 */
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String inputString) {
			// convert String into InputStream
			InputStream is = new ByteArrayInputStream(inputString.getBytes());
			br = new BufferedReader(new InputStreamReader(is));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine().toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}

	}
    

    
}

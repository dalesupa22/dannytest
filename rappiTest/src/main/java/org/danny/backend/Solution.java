package org.danny.backend;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

/**
 * @author danny 26/01/17
 */
public class Solution {
  
	//Ejecutar el main para probar la solución
	public static void main(String[] args) {
		FastScanner scan = new FastScanner();
		int t=scan.nextInt();
		for(int i=0;i<t;i++){
			int n=scan.nextInt(),m=scan.nextInt();
			HashMap<String,Integer> mapaCoordenadas=new HashMap<String,Integer>();
			for(int j=0;j<m;j++){
				String[] line=scan.nextLine().split(" ");
				if(line[0].equals("UPDATE")){
					actualizarValor(line,mapaCoordenadas);
				}else{
					System.out.println(sumarValoresCubo(line,mapaCoordenadas));
				}
			}
		}
	}
	
	
	/**
	 * Método auxiliar
	 */
	public static boolean seEncuentraEnCubo(String s,int x1,int y1,int z1,int x2,int y2,int z2){ 
		String[] com=s.split(" ");
		int x=Integer.parseInt(com[0]),y=Integer.parseInt(com[1]),z=Integer.parseInt(com[2]);
		return(x>=x1&&x<=x2&&y>=y1&&y<=y2&&z>=z1&&z<=z2);
	}
	
	/**
	 * Suma todos los valores del mapa dentro de un intervalo de coordenadas
	 * @param com
	 * @param mapaCoordenadas
	 * @return
	 */
	public static long sumarValoresCubo(String[] com,HashMap<String,Integer> mapaCoordenadas){
		int x1=Integer.parseInt(com[1]),y1=Integer.parseInt(com[2]),z1=Integer.parseInt(com[3]); //Extraccion de primera coordenada
		int x2=Integer.parseInt(com[4]),y2=Integer.parseInt(com[5]),z2=Integer.parseInt(com[6]); //Extraccion de segunda coordenada
		long sum=0;
		
		for(Entry<String, Integer> p:mapaCoordenadas.entrySet()){
			if(seEncuentraEnCubo(p.getKey(),x1,y1,z1,x2,y2,z2)){
				sum+=p.getValue();										//En caso de que el valor se encuentre en el cubo se suma el valor al total
			}
		}
		return sum;
	}
	
	/**
	 * Realiza un set de un valor determinado
	 * @param com
	 * @param mapaCoordenadas
	 */
	public static void actualizarValor(String[] com,HashMap<String,Integer> mapaCoordenadas){
		String a=com[1]+" "+com[2]+" "+com[3];
		int W=Integer.parseInt(com[4]);
		mapaCoordenadas.put(a, W); //Hace el set del valor en el mapa
	}



	//-----------FastScanner class for faster input---------------------------------- Esto fue tomado de la siguiente URL http://www.java-made-easy.com/java-scanner.html
	/**
	 * Clase auxiliar (INPUT DE DATA)
	 * @author danny
	 */
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine().toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}

	}
}
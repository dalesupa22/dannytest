package org.danny.rewrite;
import javax.servlet.ServletContext;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.*;
import org.ocpsoft.rewrite.servlet.config.*;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

/**
 * Class for URL clean rewrite
 * @author dsuarez
 * 14/02/2016
 */
@RewriteConfiguration
public class AccessRewriteConfiguration extends HttpConfigurationProvider {
	@Override
	public Configuration getConfiguration(final ServletContext context) {
		return ConfigurationBuilder.begin()
				.addRule(Join.path("/").to("/index.html"))
				.addRule(Join.path("/backEnd").to("/backend.xhtml"))
				.addRule(Join.path("/backEndAnswers").to("/docs.xhtml"));
//				.addRule(Join.path("/frontEnd").to("/front.xhtml"));
	}
	@Override
	public int priority() {
		return 10;
	}
}
package org.danny.frontend;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer categori_id;
	
	private String name;
	
	
	public Category(){
		
	}
	


	public Integer getCategori_id() {
		return categori_id;
	}





	public void setCategori_id(Integer categori_id) {
		this.categori_id = categori_id;
	}





	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	
}

package org.danny.frontend;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author danny
 */
public class UtilJson {
	
	public static <T> List<T> convertJSONToList(String valorListaJSON,
			Class<T> clasesita) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			try {
				@SuppressWarnings("static-access")
				List<T> list = mapper.readValue(valorListaJSON, mapper
						.getTypeFactory().defaultInstance()
						.constructCollectionType(List.class, clasesita));
				return list;
			} 
			catch (Exception e) {
				System.out.println("ERROR convertJSONToList Netinfo");
				e.printStackTrace();
				return null;
			}

		}
		catch (Exception e) {
			return null;
		}
	}
}

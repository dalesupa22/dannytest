/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.danny.frontend;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean
@SessionScoped
public class frontEndView {


	private String urlJSON;

	private UploadedFile file;

	private List<Product> products;
	private List<Product> filteredProducts;
	private List<Category> categoriesList=new ArrayList<Category>();
	private Product newProduct=new Product();

	private List<String> selectedCategories=new ArrayList<String>();


	@PostConstruct
	public void init() {
		//cars

	}




	public void upload() {
		if(file != null) {
			FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void handleFileUpload(FileUploadEvent event){
		try{
			file=event.getFile();
			FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			Reader reader = new InputStreamReader(file.getInputstream());
			BufferedReader br = new BufferedReader(reader);

			String line;
			StringBuilder sb = new StringBuilder();
			while((line=br.readLine())!= null){
				sb.append(line.trim());
			}

			String completeText=new String(sb.toString());
			completeText.toUpperCase();
			completeText.replaceAll("\\]\\}", "");

			System.out.println(completeText.substring(0,19));
			String[] cutText=completeText.split("\\{\"categories\"\\: \\[")[1].split(",\"products\"\\: \\[");


			String categories="["+cutText[0].replaceAll("\\]", "")+"]";
			String productsJSON="["+cutText[1].replaceAll("\\]\\}", "")+"]";

			categoriesList=UtilJson.convertJSONToList(categories, Category.class);
			products=UtilJson.convertJSONToList(productsJSON, Product.class);
			setCategoriesProducts(products,categoriesList);


		}catch(Exception e){
			FacesMessage message = new FacesMessage("Error","Error cargando el archivo");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void setCategoriesProducts(List<Product> products, List<Category> categories){
		for(Product prodTmp:products){
			for(Integer catProdTmp:prodTmp.getCategories()){
				for(Category catTmp:categories){
					if(catProdTmp==catTmp.getCategori_id()){
						prodTmp.getCategoriesAux().add(catTmp);
						break;
					}
				}
			}

		}
	}

	public String getCategoryName(Integer idCategory){
		if(idCategory!=null){
			for(Category catTmp:categoriesList){
				if(idCategory==catTmp.getCategori_id()){
					return catTmp.getName();
				}
			}
		}
		return "";
	}

	public void updateCategories(){
		newProduct.setCategoriesAux(new ArrayList<Category>());
		for(String ctTmp:selectedCategories){
			for(Category ctaTmp:categoriesList){
				if(Integer.parseInt(ctTmp)==ctaTmp.getCategori_id()){
					newProduct.getCategoriesAux().add(ctaTmp);
				}
			}
		}
	}




	public List<String> getSelectedCategories() {
		return selectedCategories;
	}




	public void setSelectedCategories(List<String> selectedCategories) {
		this.selectedCategories = selectedCategories;
	}




	public void processURLJSON(){

	}

	public void addProduct(){
		if(newProduct!=null){
			//Categories update
			newProduct.setCategoriesAux(new ArrayList<Category>());
			for(String ctTmp:selectedCategories){
				for(Category ctaTmp:categoriesList){
					if(Integer.parseInt(ctTmp)==ctaTmp.getCategori_id()){
						newProduct.getCategoriesAux().add(ctaTmp);
					}
				}
			}

			//Values setting
			Product productToAdd=new Product();
			productToAdd.setId(newProduct.getId());
			productToAdd.setName(newProduct.getName());
			productToAdd.setPrice(newProduct.getPrice());
			productToAdd.setAvailable(newProduct.getAvailable());
			productToAdd.setBest_seller(newProduct.getBest_seller());
			productToAdd.setCategories(newProduct.getCategories());
			productToAdd.setImg(newProduct.getImg());
			productToAdd.setDescription(newProduct.getDescription());
			productToAdd.setCategoriesAux(newProduct.getCategoriesAux());
			if(products!=null){
				products.add(productToAdd);
			}else{
				products=new ArrayList<Product>();
				products.add(productToAdd);
			}
		}

		newProduct=new Product();
	}

	public void deleteProduct(Product producttm){
		products.remove(producttm);
	}


	public void cleanProducts(){
		products=new ArrayList<Product>();
	}

	public void cleanAll(){
		products=new ArrayList<Product>();
		categoriesList=new ArrayList<Category>();
	}

	public String getUrlJSON() {
		return urlJSON;
	}

	public void setUrlJSON(String urlJSON) {
		this.urlJSON = urlJSON;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}



	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}




	public List<Category> getCategoriesList() {
		return categoriesList;
	}




	public void setCategoriesList(List<Category> categoriesList) {
		this.categoriesList = categoriesList;
	}




	public Product getNewProduct() {
		return newProduct;
	}




	public void setNewProduct(Product newProduct) {
		this.newProduct = newProduct;
	}




	public List<Product> getFilteredProducts() {
		return filteredProducts;
	}



	public void setFilteredProducts(List<Product> filteredProducts) {
		this.filteredProducts = filteredProducts;
	}








}

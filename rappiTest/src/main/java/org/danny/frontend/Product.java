package org.danny.frontend;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String	id;
	private String   name;
	private String  price;
	private Boolean  available;
	private Boolean   best_seller;
	private Integer[]   categories;
	private String   img;
	private String   description;
	
	private Integer  auxPrice;
	
	private List<Category> categoriesAux=new ArrayList<Category>();
	
	
	public Product(){
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
		try{
			String wf=price.replaceAll("\\W", "");
			this.auxPrice=Integer.parseInt(wf);
		}catch(Exception e){
			this.auxPrice=0;
		}
	}
	
	
	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public Boolean getBest_seller() {
		return best_seller;
	}

	public void setBest_seller(Boolean best_seller) {
		this.best_seller = best_seller;
	}


	public List<Category> getCategoriesAux() {
		return categoriesAux;
	}

	public void setCategoriesAux(List<Category> categoriesAux) {
		this.categoriesAux = categoriesAux;
	}

	public Integer[] getCategories() {
		return categories;
	}

	public void setCategories(Integer[] categories) {
		this.categories = categories;
	}

	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAuxPrice() {
		return auxPrice;
	}

	public void setAuxPrice(Integer auxPrice) {
		this.auxPrice = auxPrice;
	}





}
